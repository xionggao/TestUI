package ui;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class InfiniteListViewTest extends Application {

    private int n = 100;
    @Override
    public void start(Stage primaryStage) throws Exception {
        ObservableList<String> dataList = FXCollections.observableList(new LinkedList<>());
        IntStream.range(0, n).forEach(i-> dataList.add("demo_" + i));
        ListView<String> listView = new ListView(dataList);
        VBox vBox = new VBox();
        Button button = new Button("open");
        vBox.getChildren().addAll(button, listView);
        button.setOnAction(e->{
            VirtualFlow vf=(VirtualFlow)listView.lookup(".virtual-flow");
            System.out.println("scrollBar position:" + vf.getPosition());
            int index = vf.getFirstVisibleCell().getIndex();
            List<String> newData = IntStream.range(n, n+30).mapToObj(i-> "demo_" + i).collect(Collectors.toList());
            dataList.remove(0, 30);
            dataList.addAll(newData);
            listView.scrollTo(index);
            incr(30);
        });

        VirtualFlow vf=(VirtualFlow)listView.lookup(".virtual-flow");
        System.out.println("index:" + vf);
        Scene scene = new Scene(vBox, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void incr(int n1) {
        n += n1;
    }
}
