package common;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import ldh.common.testui.util.JsonUtil;
import ldh.common.testui.vo.JsonPathHelp;
import ldh.common.testui.vo.JsonRootHelp;
import org.junit.Test;

import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;

/**
 * Created by ldh on 2018/4/25.
 */
public class JsonPathTest {

    @Test
    public void jsonTest() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("test", 123);
        dataMap.put("test2", "hello");
        String json = JsonUtil.toJson(dataMap);
        Map<String, Object> dataMap3 = new HashMap<>();
        dataMap3.put("hht", "dasfasd");
        dataMap3.put("hh2", "1111");
        dataMap3.put("hh", json);
        String json2 = JsonUtil.toJson(dataMap3);
        System.out.println(json2);

        Object document = Configuration.defaultConfiguration().jsonProvider().parse(json2);
        String hh3 = JsonPath.read(document, "$.hh");
        System.out.println("hh3:" + hh3);
        String test2 = JsonPath.parse(hh3).read("$.test2");
        Integer test = JsonPath.parse(hh3).read("$.test");
        System.out.println("test2:" + test2 + ",test:" + test);

    }

    @Test
    public void threadTest() {
        String result = "{\n" +
                "\t\t\"id\": 1,\n" +
                "\t\t\"name\": \"common\",\n" +
                "\t\t\"parent\": {\n" +
                "\t\t\t\"id\": 0,\n" +
                "\t\t\t\"name\": \"test\",\n" +
                "\t\t\t\"parent\": null,\n" +
                "\t\t\t\"path\": null,\n" +
                "\t\t\t\"description\": null,\n" +
                "\t\t\t\"status\": null,\n" +
                "\t\t\t\"nodes\": null\n" +
                "\t\t},\n" +
                "\t\t\"path\": \"\",\n" +
                "\t\t\"description\": \"通用模块\",\n" +
                "\t\t\"status\": \"enable\",\n" +
                "\t\t\"nodes\": null\n" +
                "\t}";
        JsonPathHelp jsonPathHelp = new JsonPathHelp(result.toString(), "Json.toJson(parent.name)");  // 当前只支持返回结果是json格式的数据
        String str = jsonPathHelp.jsonValue(String.class);
        System.out.println("value:" + str);
    }
}
