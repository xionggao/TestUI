package ldh.common.testui.model;

import lombok.Data;

/**
 * Created by ldh on 2019/12/3.
 */
@Data
public class GetResultValue {

    private Long id;
    private String name;
    private String value;
    private Integer treeNodeId;
    private String type;
}
